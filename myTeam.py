# myTeam.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from captureAgents import CaptureAgent
import random, time, util
from game import Directions
import game
import distanceCalculator
from util import nearestPoint

#################
# Team creation #
#################

def createTeam(firstIndex, secondIndex, isRed,
               first = 'OffensiveTop', second = 'OffensiveBottom'):
  """
  This function should return a list of two agents that will form the
  team, initialized using firstIndex and secondIndex as their agent
  index numbers.  isRed is True if the red team is being created, and
  will be False if the blue team is being created.

  As a potentially helpful development aid, this function can take
  additional string-valued keyword arguments ("first" and "second" are
  such arguments in the case of this function), which will come from
  the --redOpts and --blueOpts command-line arguments to capture.py.
  For the nightly contest, however, your team will be created without
  any extra arguments, so you should make sure that the default
  behavior is what you want for the nightly contest.
  """

  # The following line is an example only; feel free to change it.
  return [eval(first)(firstIndex), eval(second)(secondIndex)]

##########
# Agents #
##########

class DummyAgent(CaptureAgent):
  """
  A Dummy agent to serve as an example of the necessary agent structure.
  You should look at baselineTeam.py for more details about how to
  create an agent as this is the bare minimum.
  """

  def registerInitialState(self, gameState):
    """
    This method handles the initial setup of the
    agent to populate useful fields (such as what team
    we're on). 
    
    A distanceCalculator instance caches the maze distances
    between each pair of positions, so your agents can use:
    self.distancer.getDistance(p1, p2)

    IMPORTANT: This method may run for at most 15 seconds.
    """

    ''' 
    Make sure you do not delete the following line. If you would like to
    use Manhattan distances instead of maze distances in order to save
    on initialization time, please take a look at
    CaptureAgent.registerInitialState in captureAgents.py. 
    '''
    CaptureAgent.registerInitialState(self, gameState)

    ''' 
    Your initialization code goes here, if you need any.
    '''


  def chooseAction(self, gameState):
    """
    Picks among actions randomly.
    """
    actions = gameState.getLegalActions(self.index)

    ''' 
    You should change this in your own agent.
    '''

    return random.choice(actions)

class ReflexCaptureAgent(CaptureAgent):
  """
  A base class for reflex agents that chooses score-maximizing actions
  """
  def chooseAction(self, gameState):
    """
    Picks among the actions with the highest Q(s,a).
    """
    actions = gameState.getLegalActions(self.index)

    # You can profile your evaluation time by uncommenting these lines
    # start = time.time()
    values = [self.evaluate(gameState, a) for a in actions]
    # print 'eval time for agent %d: %.4f' % (self.index, time.time() - start)

    maxValue = max(values)
    bestActions = [a for a, v in zip(actions, values) if v == maxValue]

    return random.choice(bestActions)

  def getSuccessor(self, gameState, action):
    """
    Finds the next successor which is a grid position (location tuple).
    """
    successor = gameState.generateSuccessor(self.index, action)
    pos = successor.getAgentState(self.index).getPosition()
    if pos != nearestPoint(pos):
      # Only half a grid position was covered
      return successor.generateSuccessor(self.index, action)
    else:
      return successor

  def evaluate(self, gameState, action):
    """
    Computes a linear combination of features and feature weights
    """
    features = self.getFeatures(gameState, action)
    weights = self.getWeights(gameState, action)
    return features * weights

  def getFeatures(self, gameState, action):
    """
    Returns a counter of features for the state
    """
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)
    features['successorScore'] = self.getScore(successor)
    return features

  def getWeights(self, gameState, action):
    """
    Normally, weights do not depend on the gamestate.  They can be either
    a counter or a dictionary.
    """
    return {'successorScore': 1.0}


class OffensiveTop(ReflexCaptureAgent):
  def getFeatures(self, gameState, action):
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)
    features['successorScore'] = self.getScore(successor)
    myPos = successor.getAgentState(self.index).getPosition()

    # Compute distance to the nearest food
    foodList = self.getFood(successor).asList()
    
    foodListNN = filter(lambda x: x[1] > 12, foodList)
    minDistance = 0
    #We prioritize the top 1/4 of the food on the board 
    if len(foodListNN) > 0: # This should always be True,  but better safe than sorry
      minDistance = min([self.getMazeDistance(myPos, food) for food in foodListNN])
    else:
      minDistance = min([self.getMazeDistance(myPos, food) for food in foodList])
    features['distanceToFood'] = minDistance

    #Discourage Stopping
    if action == Directions.STOP: features['stop'] = 1

    #focus on getting capsules on offense
    capsuleList = self.getCapsules(successor)
    features['capsules'] = len(capsuleList)

    
    # KILL ALL TRESSPASSERS
    #basic defensive actions for the pac man. Will go out of the way to kill invaders if close enough
    #and it's not scared. Used to gain early game advantage.
    enemies = [successor.getAgentState(i) for i in self.getOpponents(successor)]
    invaders = [a for a in enemies if a.isPacman and a.getPosition() != None]
    features['numInvaders'] = len(invaders)
    if len(invaders) > 0:
      dists = [self.getMazeDistance(myPos, a.getPosition()) for a in invaders]
      if successor.getAgentState(self.index).scaredTimer > 0:
         minDistance = min([self.getMazeDistance(myPos, food) for food in foodList])
         features['invaderDistance'] = minDistance
      else:  
         features['invaderDistance'] = min(dists)
    
    #here we define what to do if faced with an enemy ghost. If it's 3 spaces away or less it will try
    #to get away unless the enemy is scared. In that case it will ignore the ghost and eat food as usual
    ghosts = [a for a in enemies if (not a.isPacman) and a.getPosition() != None]
    if len(ghosts) > 0:
      for p in ghosts:
        if self.getMazeDistance(myPos, p.getPosition()) < 3 and p.scaredTimer == 0:
          features['died'] = 1

    return features

  def getWeights(self, gameState, action):
    return {'successorScore': 100, 'distanceToFood': -1, 'stop': -100, 'capsules': -20,'died':-500, 'invaderDistance':-100, 'ghostDistance':500}

class OffensiveBottom(ReflexCaptureAgent):
  def getFeatures(self, gameState, action):
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)
    features['successorScore'] = self.getScore(successor)
    myPos = successor.getAgentState(self.index).getPosition()

    # Compute distance to the nearest food
    foodList = self.getFood(successor).asList()

    #We prioritize the top 1/4 of the food on the board 
    foodListSS = filter(lambda x: x[1] < 4, foodList)
    minDistance = 0
    if len(foodListSS) > 0: # This should always be True,  but better safe than sorry
      minDistance = min([self.getMazeDistance(myPos, food) for food in foodListSS])
    else:
      minDistance = min([self.getMazeDistance(myPos, food) for food in foodList])
    features['distanceToFood'] = minDistance

    #Discourage Stopping
    if action == Directions.STOP: features['stop'] = 1

    #focus on getting capsules on offense
    capsuleList = self.getCapsules(successor)
    features['capsules'] = len(capsuleList)

    
    # KILL ALL TRESSPASSERS
    #basic defensive actions for the pac man. Will go out of the way to kill invaders if close enough
    #and it's not scared. Used to gain early game advantage.
    enemies = [successor.getAgentState(i) for i in self.getOpponents(successor)]
    invaders = [a for a in enemies if a.isPacman and a.getPosition() != None]
    features['numInvaders'] = len(invaders)
    if len(invaders) > 0:
      dists = [self.getMazeDistance(myPos, a.getPosition()) for a in invaders]
      if successor.getAgentState(self.index).scaredTimer > 0:
         minDistance = min([self.getMazeDistance(myPos, food) for food in foodList])
         features['invaderDistance'] = minDistance
      else:  
         features['invaderDistance'] = min(dists)
    

    #here we define what to do if faced with an enemy ghost. If it's 3 spaces away or less it will try
    #to get away unless the enemy is scared. In that case it will ignore the ghost and eat food as usual
    ghosts = [a for a in enemies if (not a.isPacman) and a.getPosition() != None]
    if len(ghosts) > 0:
      #dists = [self.getMazeDistance(myPos, a.getPosition()) for a in ghosts]
      for p in ghosts:
        #if p.getPosition() == successor.getAgentState(self.index).getPosition() and p.scaredTimer == 0:
        if self.getMazeDistance(myPos, p.getPosition()) < 3 and p.scaredTimer == 0:
          features['died'] = 1

    return features

  def getWeights(self, gameState, action):
    return {'successorScore': 100, 'distanceToFood': -1, 'stop': -100, 'capsules': -20,'died':-500, 'invaderDistance':-100, 'ghostDistance':500}
      